package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resetButton: Button
    lateinit var dice1: ImageButton
    lateinit var dice2: ImageButton


    //Dice Array
    var diceAr = arrayOf(R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3,
        R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6)


    //Toast "JACKPOT!"
    var toastText = "JACKPOT!"
    var toastTime = Toast.LENGTH_SHORT


    //Check Dice 6 & 6
    fun sixCheck(d1: Int, d2: Int){
        val toast = Toast.makeText(applicationContext, toastText, toastTime)
        if (d1 == 6 && d2 == 6) {
            toast.show()
        }
    }


    //START
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var leftDiceNum = 0
        var rightDiceNum = 0

        rollButton = findViewById(R.id.roll_button)
        resetButton = findViewById(R.id.reset_button)
        dice1 = findViewById(R.id.right_dice)
        dice2 = findViewById(R.id.left_dice)

        //Roll Button
        rollButton.setOnClickListener {
            val rollNum1 = (1..6).random()
            val rollNum2 = (1..6).random()
            dice1.setImageResource(diceAr[rollNum1 - 1])
            dice2.setImageResource(diceAr[rollNum2 - 1])

            leftDiceNum = rollNum1
            rightDiceNum = rollNum2

            sixCheck(leftDiceNum, rightDiceNum)
        }

        //Reset Button
        resetButton.setOnClickListener {
            dice1.setImageResource(R.drawable.empty_dice)
            dice2.setImageResource(R.drawable.empty_dice)
        }


        //Dice Button dice1 & dice2
        dice1.setOnClickListener {
            val diceNum1 = (1..6).random()
            dice1.setImageResource(diceAr[diceNum1 - 1])
            leftDiceNum = diceNum1
            sixCheck(leftDiceNum, rightDiceNum)
        }
        dice2.setOnClickListener {
            val diceNum2 = (1..6).random()
            dice2.setImageResource(diceAr[diceNum2 - 1])
            rightDiceNum = diceNum2
            sixCheck(leftDiceNum, rightDiceNum)
        }
    }
}


